package classSpring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class Application {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("my-spring.xml");
		//BeanFactory factory = new XmlBeanFactory(new FileSystemResource("my-spring.xml"));
		Programmer p = (Programmer)context.getBean("programmer");
		p.code();

	}

}
