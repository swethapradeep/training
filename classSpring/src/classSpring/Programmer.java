package classSpring;

public class Programmer {

	String name;
	String lastname;
	
	public Programmer(String lastname) {
		super();
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void code()
	{
		System.out.println(getName()+" "+getLastname()+" codes 1,2,3.....");
	}
}
