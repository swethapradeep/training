package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sample2 {

	public static void main(String[] args) throws IOException {
		Properties prop = new Properties();
		prop.load(new FileInputStream("D:\\casestudy4\\sample\\check.properties"));
		String pat = prop.getProperty("key3");
		Pattern pattern1 = Pattern.compile(pat);
		File f1 = new File("D:\\corejava\\MyFirstProject\\src\\Package2\\Setsam.java");
		FileReader fr = new FileReader(f1);
		BufferedReader br = new BufferedReader(fr);
		int linecount = 0;
		int commcount =0;
		String s ="";
		while((s=br.readLine())!=null)
		{
			linecount++;
			Matcher m1 = pattern1.matcher(s);
			if(m1.find())
				commcount++;
		}
		System.out.println("line "+linecount);
		System.out.println("comment "+commcount);
		System.out.println((double)linecount/commcount);
	}

}
