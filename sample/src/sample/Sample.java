package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sample {

	public static void main(String[] args) throws IOException {
	Properties prop = new Properties();
	prop.load(new FileInputStream("D:\\casestudy4\\sample\\check.properties"));
		String pat1 = prop.getProperty("key1");
		String pat2 = prop.getProperty("key2");
		File f1 = new File("D:\\corejava\\MyFirstProject\\src\\Package2\\Setsam.java");
		check(f1,pat1);
		check(f1,pat2);
		
	}
	
	public static void check(File f, String pat) throws IOException
	{
		
		Pattern pattern1 = Pattern.compile(pat);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		int count = 0;
		String s ="";
		while((s=br.readLine())!=null)
		{
			Matcher m1 = pattern1.matcher(s);
			
			if(m1.find())
				count++;
			
		}
		System.out.println(pat+" = "+count);
		br.close();
	}
	

}
